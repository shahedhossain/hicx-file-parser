package biz.shahed.boot;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import biz.shahed.hicx.file.parser.utility.ApplicationLauncher;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@EnableCaching
@EnableScheduling
@SpringBootApplication
@ComponentScan({"biz.shahed.hicx.config"})
public class BootstrapApplication extends ApplicationLauncher {

    private static final Logger LOG = LoggerFactory.getLogger(BootstrapApplication.class);
    private static final String APP_SHORT_NAME = "hicx";

    @PostConstruct
    public void init(){
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        LOG.debug("TimeZone: {}", TimeZone.getDefault());
    }

    public static void main(String[] args) throws Exception {
        launch(BootstrapApplication.class, APP_SHORT_NAME, args);
    }
}
