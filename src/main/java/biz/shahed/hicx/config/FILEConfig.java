package biz.shahed.hicx.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Configuration
@ComponentScan({ "biz.shahed.hicx.file.config" })
public class FILEConfig {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(FILEConfig.class);

}
