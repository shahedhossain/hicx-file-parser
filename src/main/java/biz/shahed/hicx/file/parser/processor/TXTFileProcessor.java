package biz.shahed.hicx.file.parser.processor;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;
import org.springframework.util.StringUtils;

import biz.shahed.hicx.file.parser.shell.ShellHelper;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class TXTFileProcessor implements FileProcessor {
    private ShellHelper shellHelper;
    private File sourceFile;
    private File targetDir;
    
    public TXTFileProcessor(ShellHelper shellHelper, File sourceFile, File targetDir) {
        this.shellHelper = shellHelper;
        this.sourceFile = sourceFile;
        this.targetDir = targetDir;
    }

    @Override
    public String read() {
        try {
            return FileUtils.readFileToString(sourceFile, Charset.defaultCharset());
        } catch (IOException ignored) {}

        return "";
    }

    @Override
    public boolean process() {
        String content = this.read();
        if (!StringUtils.isEmpty(content)) {
            int specialChars = this.countSpecialChars(content);
            int noOfWords = this.countWords(content);

            String message = sourceFile.getName()
                    .concat("\nNo.of Special Chars: ")
                    .concat(shellHelper.getOkayStyle(String.valueOf(specialChars)))
                    .concat("\nNo.of Words        : ")
                    .concat(shellHelper.getOkayStyle(String.valueOf(noOfWords)))
                    .concat("\n");

            shellHelper.print(message);
            return this.move();
        }
        return false;
    }

    @Override
    public boolean move() {
        if (!targetDir.exists()) targetDir.mkdirs();
        if (targetDir.exists() && targetDir.isDirectory()) {
            try {
                String timeSuffiedFileName = this.getTimeSuffixedFileName(sourceFile);
                File targetFile = new File(targetDir, timeSuffiedFileName);
                FileUtils.moveFile(sourceFile, targetFile);
                return true;
            } catch (IOException e) {
                shellHelper.error(e.getMessage());
            }
        } else  {
            String styledMessage = shellHelper.getErrorStyle("is not a directory!");
            String message = targetDir.getName().concat(" ").concat(styledMessage);
            shellHelper.print(message);
        }
        return false;
    }
}
