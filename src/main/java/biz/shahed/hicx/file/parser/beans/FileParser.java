package biz.shahed.hicx.file.parser.beans;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface FileParser {
    int PARSING_SIZE = 10;
    String DOC_FILES = "*.doc";
    String PDF_FILES = "*.pdf";
    String RTF_FILES = "*.rtf";
    String TXT_FILES = "*.txt";

    String[] ALL_FILES = new String[] { DOC_FILES, PDF_FILES, RTF_FILES, TXT_FILES };

    FileFilter ALL_FILES_FILTER = new WildcardFileFilter(ALL_FILES);
    FileFilter DOC_FILES_FILTER = new WildcardFileFilter(DOC_FILES);
    FileFilter PDF_FILES_FILTER = new WildcardFileFilter(PDF_FILES);
    FileFilter RTF_FILES_FILTER = new WildcardFileFilter(RTF_FILES);
    FileFilter TXT_FILES_FILTER = new WildcardFileFilter(TXT_FILES);


    void parse(File source, File target);
    File getSource();
    File getTarget();

    default List<File> allFiles() {
        return Arrays.stream(this.getSource().listFiles(ALL_FILES_FILTER))
                .collect(Collectors.toList());
    }

    default List<File> docFiles() {
        return Arrays.stream(this.getSource().listFiles(DOC_FILES_FILTER))
                .collect(Collectors.toList());
    }

    default List<File> pdfFiles() {
        return Arrays.stream(this.getSource().listFiles(PDF_FILES_FILTER))
                .collect(Collectors.toList());
    }

    default List<File> rtfFiles() {
        return Arrays.stream(this.getSource().listFiles(RTF_FILES_FILTER))
                .collect(Collectors.toList());
    }

    default List<File> txtFiles() {
        return Arrays.stream(this.getSource().listFiles(TXT_FILES_FILTER))
                .collect(Collectors.toList());
    }

    default boolean isDocFile(File file) {
        return FilenameUtils.wildcardMatch(file.getAbsolutePath(), DOC_FILES);
    }

    default boolean isPdfFile(File file) {
        return FilenameUtils.wildcardMatch(file.getAbsolutePath(), PDF_FILES);
    }

    default boolean isRtfFile(File file) {
        return FilenameUtils.wildcardMatch(file.getAbsolutePath(), RTF_FILES);
    }

    default boolean isTxtFile(File file) {
        return FilenameUtils.wildcardMatch(file.getAbsolutePath(), TXT_FILES);
    }
}
