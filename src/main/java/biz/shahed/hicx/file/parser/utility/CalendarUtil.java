package biz.shahed.hicx.file.parser.utility;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class CalendarUtil {
    private static Logger LOG = LoggerFactory.getLogger(CalendarUtil.class);

    public static GregorianCalendar gregorian(Date date) {
        GregorianCalendar gregorian = new GregorianCalendar();
        gregorian.setTime(date);
        return gregorian;
    }

    public static GregorianCalendar gregorian(String date, String pattern) {
        return gregorian(DateUtil.parse(date, pattern));
    }

    public static XMLGregorianCalendar gregorianXML(Date date) {
        return gregorianXML(gregorian(date));
    }

    public static XMLGregorianCalendar gregorianXML(String date, String pattern) {
        return gregorianXML(gregorian(date, pattern));
    }

    public static XMLGregorianCalendar gregorianXML(GregorianCalendar gregorian) {
        XMLGregorianCalendar gregorianXML = null;
        try {
            gregorianXML = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorian);
        } catch (DatatypeConfigurationException e) {
            LOG.error(e.getMessage(), e);
        }
        return gregorianXML;
    }
}
