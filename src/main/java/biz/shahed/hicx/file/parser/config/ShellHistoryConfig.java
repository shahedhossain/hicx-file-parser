package biz.shahed.hicx.file.parser.config;

import java.io.IOException;
import java.nio.file.Paths;

import org.jline.reader.History;
import org.jline.reader.LineReader;
import org.jline.reader.impl.history.DefaultHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
//@Configuration
public class ShellHistoryConfig {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(ShellHistoryConfig.class);

    @Autowired
    private History history;

    @Bean
    public History defaultHisoty(LineReader lineReader, @Value("${hicx.app.history}") String historyPath) {
        lineReader.setVariable(LineReader.HISTORY_FILE, Paths.get(historyPath));
        return new DefaultHistory(lineReader);
    }

    @EventListener
    public void onContextClosedEvent(ContextClosedEvent event) throws IOException {
        history.save();
    }
}
