package biz.shahed.hicx.file.parser.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class ApplicationLauncher {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(ApplicationLauncher.class);

    public static void launch(Class<?> clazz, String shortName, String[] args) throws Exception {
        SpringApplication apps = new SpringApplication(clazz);
        apps.addListeners(new BootstrapPIDListener(shortName));
        apps.run(args);
    }
}
