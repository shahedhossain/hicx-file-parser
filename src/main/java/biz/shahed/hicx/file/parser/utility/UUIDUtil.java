package biz.shahed.hicx.file.parser.utility;

import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class UUIDUtil {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(UUIDUtil.class);

    public static UUID uuid() {
        Date date = new Date();
        return uuid(date);
    }

    public static UUID uuid(Date date) {
        return new java.util.UUID(date.getTime(), 0);
    }

    public static UUID uuid(String uuid) {
        return UUID.fromString(uuid);
    }

    public static long timestamp(String uuid) {
        return uuid(uuid).timestamp();
    }

    public static Date date(String uuid) {
        return new Date(timestamp(uuid));
    }
}
