package biz.shahed.hicx.file.parser.beans.command;

import biz.shahed.hicx.file.parser.beans.FileParser;
import biz.shahed.hicx.file.parser.shell.ShellHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.io.File;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@ShellComponent
public class FileParserCommand {
    @Autowired
    ShellHelper shellHelper;

    @Autowired
    FileParser fileParser;

    @ShellMethod("Parse files of a absolute/relative path.")
    public void parse(@ShellOption({ "-p", "--path"}) String path) {
        File source = new File(path);
        if (source.exists() && source.isDirectory()) {
            File target = new File(source, "processed");
            if (!target.exists()) target.mkdirs();
            fileParser.parse(source, target);
        } else {
            String styledMessage = shellHelper.getErrorStyle("invalid path!");
            String message = path.concat(" ").concat(styledMessage);
            shellHelper.print(message);
        }
    }
}
