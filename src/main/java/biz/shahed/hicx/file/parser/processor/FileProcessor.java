package biz.shahed.hicx.file.parser.processor;

import java.io.File;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.commons.io.FilenameUtils;
import org.springframework.util.StringUtils;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public interface FileProcessor {
    String read();
    boolean process();
    boolean move();

    default int countSpecialChars(String content) {
        int counter = 0;
        if (!StringUtils.isEmpty(content)) {
            for (int index = 0; index < content.length(); index++) {
                char c = content.charAt(index);
                if (!Character.isDigit(c) && !Character.isLetter(c) && !Character.isWhitespace(c)) {
                    counter++;
                }
            }
        }
        return counter;
    }

    default int countWords(String content) {
        int counter = 0;
        if (!StringUtils.isEmpty(content)) {
            StringTokenizer tokens = new StringTokenizer(content);
            counter = tokens.countTokens();
        }
        return counter;
    }

    default String getTimeSuffixedFileName(File sourceFile) {
        long millis = (new Date()).getTime();
        String absolutePath = sourceFile.getAbsolutePath();
        String baseName = FilenameUtils.getBaseName(absolutePath);
        String extension = FilenameUtils.getExtension(absolutePath);
        String fileName = String.format("%s__%d.%s", baseName, millis, extension);
        return fileName;
    }
}
