package biz.shahed.hicx.file.parser.service.observer;

import java.util.Observable;
import java.util.Observer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import biz.shahed.hicx.file.parser.shell.ProgressBar;
import biz.shahed.hicx.file.parser.shell.ShellHelper;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Service
public class ProgressUpdateObserver implements Observer {

    @Autowired
    private ProgressBar progressBar;

    @Autowired
    private ShellHelper shellHelper;

    @Override
    public void update(Observable observable, Object event) {
        ProgressUpdateEvent upe = (ProgressUpdateEvent) event;
        int currentRecord = upe.getCurrentCount().intValue();
        int totalRecords = upe.getTotalCount().intValue();

        if (currentRecord == 0) {
            // just in case the previous progress bar was interrupted
            progressBar.reset();
        }

        String message = null;
        int percentage = currentRecord * 100 / totalRecords;
        if (StringUtils.hasText(upe.getMessage())) {
            message = shellHelper.getWarnStyle(upe.getMessage());
            progressBar.display(percentage, message);
        }

        progressBar.display(percentage, message);
        if (percentage == 100) {
            progressBar.reset();
        }
    }
}
