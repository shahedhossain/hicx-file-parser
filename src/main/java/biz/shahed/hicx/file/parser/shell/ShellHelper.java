package biz.shahed.hicx.file.parser.shell;

import org.jline.terminal.Terminal;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class ShellHelper {
    public static final int WARN = AttributedStyle.YELLOW;
    public static final int OKAY = AttributedStyle.GREEN;
    public static final int INFO = AttributedStyle.CYAN;
    public static final int ERROR = AttributedStyle.RED;

    private Terminal terminal;

    public ShellHelper(Terminal terminal) {
        this.terminal = terminal;
    }

    public void print(String message) {
        terminal.writer().println(message);
        terminal.flush();
    }

    public void info(String message) {
        print(message, INFO);
    }

    public void error(String message) {
        print(message, ERROR);
    }

    public void warn(String message) {
        print(message, WARN);
    }

    public void okay(String message) {
        print(message, OKAY);
    }

    public void print(String message, int color) {
        terminal.writer().println(getAnsiStyle(message, color));
        terminal.flush();
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public String getAnsiStyle(String message, int color) {
        return (new AttributedStringBuilder()).append(message, AttributedStyle.DEFAULT.foreground(color)).toAnsi();
    }

    public String getInfoStyle(String message) {
        return getAnsiStyle(message, INFO);
    }

    public String getOkayStyle(String message) {
        return getAnsiStyle(message, OKAY);
    }

    public String getWarnStyle(String message) {
        return getAnsiStyle(message, WARN);
    }

    public String getErrorStyle(String message) {
        return getAnsiStyle(message, ERROR);
    }
}
