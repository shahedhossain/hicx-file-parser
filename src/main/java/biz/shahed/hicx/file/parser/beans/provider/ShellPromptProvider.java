package biz.shahed.hicx.file.parser.beans.provider;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.stereotype.Component;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class ShellPromptProvider implements PromptProvider {
    public static final AttributedStyle PROMPT_STYLE = AttributedStyle.DEFAULT.foreground(AttributedStyle.GREEN).bold();

    @Value("${user.name}")
    String userName;

    @Override
    public AttributedString getPrompt() {
        String prompt = String.format("%s@file/parser:>", userName);
        return new AttributedString(prompt, PROMPT_STYLE);
    }
}
