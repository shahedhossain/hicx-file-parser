package biz.shahed.hicx.file.parser.config;

import org.jline.reader.History;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.Parser;
import org.jline.terminal.Terminal;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.shell.jline.JLineShellAutoConfiguration.CompleterAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;

import biz.shahed.hicx.file.parser.shell.InputReader;
import biz.shahed.hicx.file.parser.shell.ProgressBar;
import biz.shahed.hicx.file.parser.shell.ProgressCounter;
import biz.shahed.hicx.file.parser.shell.ShellHelper;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Configuration
@ComponentScan({"biz.shahed.hicx.file.parser.beans"})
public class SpringBeansConfig {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(SpringBeansConfig.class);

    @Bean
    public ShellHelper shellHelper(@Lazy Terminal terminal) {
        return new ShellHelper(terminal);
    }

    @Bean
    public InputReader inputReader(
            @Lazy Terminal terminal,
            @Lazy Parser parser,
            CompleterAdapter completer,
            @Lazy History history,
            ShellHelper shellHelper) {

        LineReaderBuilder lineReaderBuilder = LineReaderBuilder.builder()
                .terminal(terminal).completer(completer)
                .history(history).highlighter((LineReader reader, String buffer) -> {
                    return new AttributedString(buffer, AttributedStyle.BOLD.foreground(AttributedStyle.WHITE));
                }).parser(parser);

        LineReader lineReader = lineReaderBuilder.build();
        lineReader.unsetOpt(LineReader.Option.INSERT_TAB);
        return new InputReader(lineReader, shellHelper);
    }

    @Bean
    public ProgressBar progressBar(ShellHelper shellHelper) {
        return new ProgressBar(shellHelper);
    }

    @Bean
    public ProgressCounter progressCounter(@Lazy Terminal terminal) {
        return new ProgressCounter(terminal);
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
