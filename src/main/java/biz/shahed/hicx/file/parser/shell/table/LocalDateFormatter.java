package biz.shahed.hicx.file.parser.shell.table;

import org.springframework.shell.table.Formatter;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class LocalDateFormatter implements Formatter {
    private String pattern;

    public LocalDateFormatter(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public String[] format(Object value) {
        LocalDate localDate = (LocalDate) value;
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return new String[] { format.format(date) };
    }
}
