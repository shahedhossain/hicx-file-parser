package biz.shahed.hicx.file.parser.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import biz.shahed.hicx.file.parser.processor.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import biz.shahed.hicx.file.parser.shell.ShellHelper;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
@Component
public class SimpleFileParser implements FileParser {
    @Autowired
    ShellHelper shellHelper;
    private File source;
    private File target;

    @Override
    public void parse(File source, File target) {
        this.setSource(source);
        this.setTarget(target);

        if (!CollectionUtils.isEmpty(this.allFiles())) {
            List<FileProcessor> processors = this.process();
            int noOfProcessors = processors.size();

            for (int index = 0; index < noOfProcessors; index += PARSING_SIZE) {
                List<FileProcessor> offset = processors.subList(index, Math.min(noOfProcessors, index + PARSING_SIZE));
                for(FileProcessor processor : offset){
                    try {
                        Thread t = new Thread(processor::process);
                        t.start();
                        t.join();
                    } catch (InterruptedException e) {
                        shellHelper.error(e.getMessage());
                    }
                }
            }
        } else {
            String styledMessage = shellHelper.getWarnStyle("is empty/files aren't supported!");
            String message = source.getName().concat(" ").concat(styledMessage);
            shellHelper.print(message);
        }
    }

    private List<FileProcessor> process() {
        List<FileProcessor> processors = new ArrayList<>();
        this.docFiles().forEach(doc -> processors.add(new DOCFileProcessor(shellHelper, doc, getTarget())));
        this.pdfFiles().forEach(pdf -> processors.add(new PDFFileProcessor(shellHelper, pdf, getTarget())));
        this.rtfFiles().forEach(rtf -> processors.add(new RTFFileProcessor(shellHelper, rtf, getTarget())));
        this.txtFiles().forEach(txt -> processors.add(new TXTFileProcessor(shellHelper, txt, getTarget())));
        return processors;
    }

    public File getSource() {
        return source;
    }

    protected void setSource(File source) {
        this.source = source;
    }

    public File getTarget() {
        return target;
    }

    protected void setTarget(File target) {
        this.target = target;
    }
}
