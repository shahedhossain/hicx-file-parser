package biz.shahed.hicx.file.parser.utility;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.context.event.ApplicationStartingEvent;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class BootstrapPIDListener extends ApplicationPidFileWriter {
    private static final Logger LOG = LoggerFactory.getLogger(BootstrapPIDListener.class);
    private static final String PID_PATH_FORMAT = "%s/.shahed/hicx/etc/pid/%s.pid".replace('/', File.separatorChar);

    public BootstrapPIDListener() {
        super();
        setTriggerEventType(ApplicationStartingEvent.class);
    }

    public BootstrapPIDListener(File file) {
        super(file);
        setTriggerEventType(ApplicationStartingEvent.class);
    }

    public BootstrapPIDListener(String proname) {
        super(getAppPIDFile(proname));
        setTriggerEventType(ApplicationStartingEvent.class);
    }

    public static File getAppPIDFile(String appsShortName){
        String userhome = System.getProperty("user.home");
        String pathname = String.format(PID_PATH_FORMAT, userhome, appsShortName);
        LOG.debug("PID File Path: {}", pathname);
        return new File(pathname);
    }
}
