package biz.shahed.hicx.file.parser.test.utility;

import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.hicx.file.parser.utility.DateUtil;

/**
 * @author    Shahed<devs@shahed.biz>
 * @version   1.0.00.GA
 * @since     1.0.00.GA
 */
public class DateUtilTest {
    @SuppressWarnings("unused")
    private static final Logger log = LoggerFactory.getLogger(DateUtilTest.class);

    @SuppressWarnings("unused")
    private String parseDate = "20140916090344.637+0600"; //+0600";
    private Date date;

    @BeforeEach
    public void setUp() throws Exception {
        date = DateUtil.parse("20140916052234.830+0700", "yyyyMMddHHmmss.S");
        //log.info("HL7 Date : {}",  DateUtil.format(date, DateUtil.IHEHL7_DATE));
    }

    @Test
    public void testParse() throws Exception {
        Date date = DateUtil.parse("20140101010101");
        Assertions.assertEquals((new GregorianCalendar(2014, 0, 1, 1, 1, 1)).getTime(), date);

        date = DateUtil.parse("20140101010101.110");
        Assertions.assertNotEquals((new GregorianCalendar(2014, 0, 1, 1, 1, 1)).getTime().getTime(), date.getTime());
        Assertions.assertEquals((new GregorianCalendar(2014, 0, 1, 1, 1, 1)).getTime().getTime() + 110, date.getTime());
    }

    //@Test
    public void testFormat() throws Exception {
        date = DateUtil.parse("20140101010101.110+0600");
        Assertions.assertEquals("20140101010101.110+0600", DateUtil.format(date, DateUtil.HL7_DATE_FORMAT));
    }
}
