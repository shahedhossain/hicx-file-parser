FROM openjdk:8-jdk-alpine
ENV CHORKE_HOME='/var/chorke'\
 SPRING_PROFILES_ACTIVE=docker\
 CHORKE_DS_POOLNAME='java:jboss/datasources/H2_file_parser_devDS'\
 CHORKE_DS_JDBC_URL='jdbc:h2:file:${user.home}.shahed/hicx/var/h2/${hicx.datasource.database};\
db_close_on_exit=false;mode=MySQL;user=${hicx.datasource.username};password=${hicx.datasource.password}'\
 CHORKE_DS_DBDRIVER='org.h2.Driver'\
 CHORKE_DS_DATABASE='hicx'\
 CHORKE_DS_USERNAME='hicx'\
 CHORKE_DS_PASSWORD='hicx'\
 CHORKE_DS_SQLQUERY='SELECT 1'\
 CHORKE_H2_WEBADMIN='false'\
 CHORKE_H2_ALLOWALL='false'\
 CHORKE_JPA_DIALECT='org.hibernate.dialect.H2Dialect'\
 CHORKE_JPA_SHOWSQL='false'\
 CHORKE_GQL_ENABLED='true'\
 CHORKE_GQL_BROWSER='true'\
 CHORKE_LIQ_ENABLED='true'\
 CHORKE_LIQ_CONTEXT='dev'\
 CHORKE_LOG_ROLLING='WARN'\
 CHORKE_LOG_CONSOLE='OFF'\
 CHORKE_REQ_CONTEXT='/parser'

RUN mkdir $CHORKE_HOME &&\
 chmod 644 -R $CHORKE_HOME &&\
 apk add --no-cache curl &&\
 curl -s http://10.19.83.110:5000/hicx-file-parser.jar -o $CHORKE_HOME/hicx-file-parser.jar &&\
 chmod 755 $CHORKE_HOME/hicx-file-parser.jar

VOLUME ["$HOME/.shahed/hicx"]
WORKDIR $CHORKE_HOME
EXPOSE 1983

ENTRYPOINT ["java", "-jar", "hicx-file-parser.jar"]
