# [hicx-file-parser][201]

Most of the java projects of [Shahed, Inc.][000]'s are mavenized, it's a maven `jar` project. This is the child of the [hicx-file-parent][202]. This module developed aim to support bootstrap/client application. It will be package as `jar`. The dependent project can be use it as a dependency by following:


```xml
<dependency>
    <artifactId>hicx-file-parser</artifactId>
    <groupId>biz.shahed.hicx.java.file.parser</groupId>
    <version>1.0.00.GA</version>
</dependency>
```


All of the resources and libraries of [Shahed, Inc.][000] permitted to use under considering `MIT` license. Clone this project using Git Source Control Manager:

1. [`git clone bit.shahed.biz:shahedhossain/hicx-file-parser.git`][201] from [bit.shahed.biz][200]


# upload `rsa` keys then add to `~/.ssh/config`

```cfg
Host bit.shahed.biz
     HostName bitbucket.org
     PreferredAuthentications publickey
     IdentityFile ~/.ssh/bit_shahed_biz_rsa
     User git
```

# build

```bash
mvn spring-boot:run
mvn clean install -U
mvn clean install -Dmaven.test.skip
```

# Run
```bash
mvn clean install spring-boot:run -U
:<<EOF
     ...
   xHCC"`~ .xCX      .uef^"                            < .z@K"`
 :CCCC   .f"CCCCHf :dHHE              u.      .u    .   !@KKE
:CCCC>  XCL  ^""`  `HHHE        ...ueOOOb   .dRRR :@Rc  `KKKE   u         .u
XCCCC  XCCCh        HHHE .zHk   OOOR YOOOr ="RRRRfRRRRr  KKKE u@KNL    udEEEE.
CCCCC  !CCCCC.      HHHE~?HHHL  OOOR IOOO>   4RRR>`RR"   KKKE`"KK*"  :EEE`EEEE.
CCCCC   %CCCCC      HHHE  HHHE  OOOR IOOO>   4RRR> `     KKKE .dN.   dEEE `EE%"
CCCCC `> `CCCC>     HHHE  HHHE  OOOR IOOO>   4RRR>       KKKE~KKKK   EEEE.+"
`CCCCL %  ?CCC   !  HHHE  HHHE uOOOOcJOOO   .dRRRL .+    KKKE `KKK&  EEEEL
 `CCCC  `-*""   /   HHHE  HHHE  "*OOO*P"    ^"RRRR*"     KKKE  9KKK. `EEEEc. .+
   "CCC.      :"   mHHHN= HHH>    `Y"          "Y"     `"KKK*" 4KKK"  "EEEEE%
     `""***~"`      `Y"   HHH                             ""    ""      "YP`
                         JHH"
Copyright:               @%
@2013~2021 Chorke, Inc.:"
hicx/file/parser/1.0.00.GA
================================================================================
shahed@file/parser:>parse  /Users/shahed/Downloads/ebooks
Python Scripting for Computational Science (2008).pdf
No.of Special Chars: 3660165
No.of Words        : 265470

Oracle 10g SQL Funda-II.pdf
No.of Special Chars: 1329919
No.of Words        : 248316

Oracle 10g SQL Funda-I.pdf
No.of Special Chars: 2266423
No.of Words        : 465848

rows.sh.txt
No.of Special Chars: 577
No.of Words        : 272

shahed@file/parser:>parse  /Users/shahed/Downloads/ebooks
ebooks is empty/files aren't supported!
shahed@file/parser:>exit
EOF
```


# LICENSE

```
Copyright (c) 2013-2021 Shahed, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```


# Project

Practically it's a utility or sample project. `Academian` may use this project under considering `MIT|GPLv3` license. Infact this project started aim to build the foundation of enterprise graded project. Base on this library an enthusiast will be able to build enterprise graded application. This library project will be reduce the learning curve and maximized the focus on productivity.


# Contributor

**Shahed Bin Abdullah** is the founder of **Chorke Academia, Inc.** He is an `Academian` and `Geekery` of `Academia`. In his `sultanate` **Rashida** is his only `sultana` and **Raiyan** is their only `shehzada`. Who are the part of inspiration, innovation and contribution to **Chorke Academia, Inc.**


# Chorke Academia, Inc


**Chorke Academia, Inc** founded aimed to develop `Open Source Enterprise Application` based on `Java`, `JavaScript`, `TypeScript`, `Python`, `Swift`, `Ruby`, `Perl`, `PHP`, `C#` and `C++`. Integrated, Modularized, Simplified and Loosely coupled application development is our vision. Our goal is minimizing the learning curve, maximizing the productivity by using `Open Source Technology`. `Chorke Academia, Inc.` is trying to maintain the best practice for transparent, robust and enrich software development by using `OOP`. Our development process is optimized. What's accelerated by `XP`, `Agile Scrum Master`, `CI` and `CD`.


# Contact

- [**devs@shahed.biz**][100]
- [**shahed.biz**][000]


[000]:  https://shahed.biz "Visit us"
[100]:  mailto:devs@shahed.biz "Email us"

[200]:  https://bitbucket.org/shahedhossain "Shahed"
[201]:  https://bitbucket.org/shahedhossain/hicx-file-parser "hicx-file-parser"
[202]:  https://bitbucket.org/shahedhossain/hicx-file-parent "hicx-file-parent"
